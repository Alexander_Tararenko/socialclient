package com.gmail.atararenko.socialclient.presentation.view.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.TextView;

import com.gmail.atararenko.socialclient.R;
import com.gmail.atararenko.socialclient.model.PostModel;

import java.util.List;

public class ViewPagerAdapter extends PagerAdapter {

    private Context mContext;
    private List<PostModel> mPosts;
    private ViewHolder[] mViewHolders;
    private OnPostClickListener mCallback;

    public ViewPagerAdapter(final Context mContext, List<PostModel> posts) {
        this.mContext = mContext;
        mViewHolders = new ViewHolder[6];
        this.mPosts = posts;
    }

    @Override
    public int getCount() {
        return mPosts.size() % 6 == 0 ? mPosts.size() / 6 : mPosts.size() / 6 + 1;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((GridLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.pager_item, container, false);
        int i = 0;
        mViewHolders[i++] = initViewHolder(itemView, R.id.post_0);
        mViewHolders[i++] = initViewHolder(itemView, R.id.post_1);
        mViewHolders[i++] = initViewHolder(itemView, R.id.post_2);
        mViewHolders[i++] = initViewHolder(itemView, R.id.post_3);
        mViewHolders[i++] = initViewHolder(itemView, R.id.post_4);
        mViewHolders[i] = initViewHolder(itemView, R.id.post_5);
        fillPosts(position);
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((GridLayout) object);
    }

    public void setCallback(OnPostClickListener callback) {
        this.mCallback = callback;
    }

    public void setPosts(List<PostModel> posts) {
        this.mPosts = posts;
        this.notifyDataSetChanged();
    }

    private ViewHolder initViewHolder(View view, int id) {
        return new ViewHolder(view.findViewById(id));
    }

    private void fillPosts(int position) {
        for (int i = 0; i < 6; i++) {
            int index = 6 * position + i;
            if (index >= mPosts.size()) {
                mViewHolders[i].mContainerView.setVisibility(View.GONE);
            } else {
                mViewHolders[i].mContainerView.setVisibility(View.VISIBLE);
                mViewHolders[i].setPost(mPosts.get(index));
            }
        }
    }

    public interface OnPostClickListener {
        void onClick(PostModel post);
    }

    // Not actually a ViewHolder pattern
    private class ViewHolder {
        View mContainerView;
        TextView mPostNumber;
        TextView mPostTitle;
        PostModel post;

        public ViewHolder(View view) {
            mContainerView = view;
            mPostNumber = (TextView) view.findViewById(R.id.post_number_text_view);
            mPostTitle = (TextView) view.findViewById(R.id.post_title_text_view);
        }

        public void setPost(final PostModel post) {
            this.post = post;
            mPostNumber.setText(Long.toString(post.getId()));
            if (post.getTitle() != null) {
                mPostTitle.setText(post.getTitle());
            }
            mContainerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mCallback != null) {
                        mCallback.onClick(post);
                    }
                }
            });
        }

    }

}
