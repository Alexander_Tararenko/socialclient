package com.gmail.atararenko.socialclient.presentation.presenter.asynctask;

import android.os.AsyncTask;

import com.gmail.atararenko.socialclient.domain.json.IPostParser;
import com.gmail.atararenko.socialclient.domain.json.PostParser;
import com.gmail.atararenko.socialclient.model.PostModel;
import com.gmail.atararenko.socialclient.presentation.view.IPostListView;

import java.io.IOException;
import java.util.List;

public class LoadPostsTask extends AsyncTask<Void, Void, List<PostModel>> {

    private static final String POST_URL = "http://jsonplaceholder.typicode.com/posts";
    private IPostListView mView;

    public LoadPostsTask(IPostListView view) {
        this.mView = view;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mView.showDownloadingPostProgress();
    }

    @Override
    protected List<PostModel> doInBackground(Void... voids) {
        IPostParser parser = new PostParser();
        List<PostModel> results;
        try {
            results = parser.parse(POST_URL);
        } catch (IOException e) {
            return null;
        }
        return results;
    }

    @Override
    protected void onPostExecute(List<PostModel> posts) {
        super.onPostExecute(posts);
        if (posts == null) {
            mView.showDownloadingPostErrorMessage();
        } else {
            mView.hideDownloadingPostProgress();
            mView.showPosts(posts);
        }
    }
}
