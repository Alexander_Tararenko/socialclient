package com.gmail.atararenko.socialclient.presentation.presenter.asynctask;

import android.os.AsyncTask;

import com.gmail.atararenko.socialclient.domain.json.IUserParser;
import com.gmail.atararenko.socialclient.domain.json.UserParser;
import com.gmail.atararenko.socialclient.model.UserModel;
import com.gmail.atararenko.socialclient.presentation.view.IUserDetailsView;

import java.io.IOException;

public class LoadUserDataTask extends AsyncTask<Long, Void, UserModel> {

    private static final String USER_URL = "http://jsonplaceholder.typicode.com/users/";
    private IUserDetailsView mView;

    public LoadUserDataTask(IUserDetailsView view) {
        this.mView = view;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected UserModel doInBackground(Long... ids) {
        IUserParser parser = new UserParser();
        UserModel result;
        try {
            result = parser.parse(USER_URL + ids[0]);
        } catch (IOException e) {
            return null;
        }
        return result;
    }

    @Override
    protected void onPostExecute(UserModel user) {
        super.onPostExecute(user);
        if (user != null) {
            mView.showUser(user);
        } else {
            mView.showConnectionErrorMessage();
        }
    }
}
