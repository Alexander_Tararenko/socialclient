package com.gmail.atararenko.socialclient.presentation.presenter.asynctask;

import android.os.AsyncTask;

import com.gmail.atararenko.socialclient.presentation.view.IPostListView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public class SaveLogTask extends AsyncTask<Void, Void, Void> {

    private IPostListView mView;

    public SaveLogTask(IPostListView view) {
        mView = view;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mView.showSaveLogProgress();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        File file = createFile();
        saveLogToFile(file);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        mView.hideSaveLogProgress();
        mView = null;
    }

    private void saveLogToFile(final File file) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(file));
            StringBuilder stringBuilder = getLogcat();
            writer.write(stringBuilder.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private StringBuilder getLogcat() throws IOException {
        Process process = Runtime.getRuntime().exec("logcat -d");
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(process.getInputStream()));
        StringBuilder log = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null && line.length() != 0) {
            log.append(line);
            log.append("\n");
        }
        bufferedReader.close();
        return log;
    }

    private File createFile() {
        File root = android.os.Environment.getExternalStorageDirectory();
        File dir = new File(root.getAbsolutePath() + "/SocialClient");
        dir.mkdirs();
        return new File(dir, "/logcat.txt");
    }

}
