package com.gmail.atararenko.socialclient.domain.json;

import com.gmail.atararenko.socialclient.model.PostModel;

import java.io.IOException;
import java.util.List;

public interface IPostParser {

    List<PostModel> parse(String url) throws IOException;

}
