package com.gmail.atararenko.socialclient.presentation.presenter;

import android.content.Context;
import android.content.pm.PackageManager;

import com.gmail.atararenko.socialclient.presentation.presenter.asynctask.LoadPostsTask;
import com.gmail.atararenko.socialclient.presentation.presenter.asynctask.SaveLogTask;
import com.gmail.atararenko.socialclient.presentation.view.IPostListView;

public class PostListPresenter implements IPostListPresenter {

    private IPostListView mView;
    private Context mContext;

    public PostListPresenter(IPostListView mView, Context context) {
        this.mView = mView;
        this.mContext = context;
    }

    @Override
    public void saveLog() {
        if (checkWriteExternalPermission()) {
            SaveLogTask task = new SaveLogTask(mView);
            task.execute();
        } else {
            mView.requestWriteExternalStoragePermission();
        }
    }

    @Override
    public void getPosts() {
        // TODO: check internet connection first
        LoadPostsTask task = new LoadPostsTask(mView);
        task.execute();
    }

    private boolean checkWriteExternalPermission() {
        String permission = "android.permission.WRITE_EXTERNAL_STORAGE";
        int res = mContext.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

}
