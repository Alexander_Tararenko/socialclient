package com.gmail.atararenko.socialclient.presentation.presenter;

import android.content.Context;

import com.gmail.atararenko.socialclient.data.database.DatabaseManager;
import com.gmail.atararenko.socialclient.model.UserModel;
import com.gmail.atararenko.socialclient.presentation.presenter.asynctask.LoadUserDataTask;
import com.gmail.atararenko.socialclient.presentation.view.IUserDetailsView;

public class UserDetailsPresenter implements IUserDetailsPresenter {

    private IUserDetailsView mView;
    private Context mContext;

    public UserDetailsPresenter(IUserDetailsView view, Context context) {
        this.mView = view;
        this.mContext = context;
    }

    @Override
    public void getUserData(long userId) {
        LoadUserDataTask task = new LoadUserDataTask(mView);
        task.execute(userId);
    }

    @Override
    public void saveUserToDatabase(UserModel user) {
        if (user != null) {
            DatabaseManager manager = new DatabaseManager(mContext);
            manager.addUser(user);
        }
        mView.showSaveToDBSuccessMessage();
    }
}
