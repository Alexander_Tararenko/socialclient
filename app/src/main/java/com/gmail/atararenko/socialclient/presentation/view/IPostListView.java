package com.gmail.atararenko.socialclient.presentation.view;

import com.gmail.atararenko.socialclient.model.PostModel;

import java.util.List;

public interface IPostListView {

    void showPosts(List<PostModel> posts);

    void requestWriteExternalStoragePermission();

    void showSaveLogProgress();

    void hideSaveLogProgress();

    void showDownloadingPostProgress();

    void hideDownloadingPostProgress();

    void showDownloadingPostErrorMessage();

}
