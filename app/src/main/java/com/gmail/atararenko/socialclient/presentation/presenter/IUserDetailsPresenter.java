package com.gmail.atararenko.socialclient.presentation.presenter;

import com.gmail.atararenko.socialclient.model.UserModel;

public interface IUserDetailsPresenter {

    void getUserData(long userId);

    void saveUserToDatabase(UserModel user);

}
