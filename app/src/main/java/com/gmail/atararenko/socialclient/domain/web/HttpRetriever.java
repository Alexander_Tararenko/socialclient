package com.gmail.atararenko.socialclient.domain.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class HttpRetriever {

    public static String retrieve(String url, int timeoutInSeconds) throws IOException {
        StringBuilder builder = new StringBuilder();
        URLConnection connection = (new URL(url)).openConnection();
        connection.setConnectTimeout(timeoutInSeconds * 1000);
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }
        reader.close();
        return builder.toString();
    }

}
