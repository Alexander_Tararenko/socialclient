package com.gmail.atararenko.socialclient.data.database;

import com.gmail.atararenko.socialclient.data.database.table.AddressesTable;
import com.gmail.atararenko.socialclient.data.database.table.CompaniesTable;
import com.gmail.atararenko.socialclient.data.database.table.UsersTable;

public interface SQL {

    String TEXT_TYPE = " TEXT";
    String INTEGER_TYPE = " INTEGER";
    String REAL_TYPE = " REAL";
    String INTEGER_PRIMARY_KEY = " INTEGER PRIMARY KEY";
    String PRIMARY_KEY = "  PRIMARY KEY";
    String INTEGER_PRIMARY_KEY_AUTO_INCREMENT = " INTEGER PRIMARY KEY AUTOINCREMENT";
    String DEFAULT = " DEFAULT";
    String CURRENT_TIMESTAMP = " CURRENT_TIMESTAMP";
    String COMMA_SEP = ", ";

    String CREATE_TABLE_USERS = "CREATE TABLE "
            + UsersTable.TABLE_NAME + " ("
            + UsersTable.COLUMN_USER_ID + INTEGER_PRIMARY_KEY + COMMA_SEP
            + UsersTable.COLUMN_USER_NAME + TEXT_TYPE + COMMA_SEP
            + UsersTable.COLUMN_USER_USERNAME + TEXT_TYPE + COMMA_SEP
            + UsersTable.COLUMN_USER_EMAIL + TEXT_TYPE + COMMA_SEP
            + UsersTable.COLUMN_USER_WEBSITE + TEXT_TYPE + COMMA_SEP
            + UsersTable.COLUMN_USER_PHONE + TEXT_TYPE + COMMA_SEP
            + UsersTable.COLUMN_NULLABLE + TEXT_TYPE
            + ")";

    String DELETE_TABLE_USERS = "DROP TABLE IF EXISTS "
            + UsersTable.TABLE_NAME;

    String CREATE_TABLE_COMPANIES = "CREATE TABLE "
            + CompaniesTable.TABLE_NAME + " ("
            + CompaniesTable.COLUMN_COMPANY_NAME + TEXT_TYPE + PRIMARY_KEY + COMMA_SEP
            + CompaniesTable.COLUMN_COMPANY_CATCHPHRASE + TEXT_TYPE + COMMA_SEP
            + CompaniesTable.COLUMN_COMPANY_BS + TEXT_TYPE + COMMA_SEP
            + CompaniesTable.COLUMN_NULLABLE + TEXT_TYPE
            + ")";

    String DELETE_TABLE_COMPANIES = "DROP TABLE IF EXISTS "
            + CompaniesTable.TABLE_NAME;

    String CREATE_TABLE_ADDRESSES = "CREATE TABLE "
            + AddressesTable.TABLE_NAME + " ("
            + AddressesTable.COLUMN_ADDRESS_ID + INTEGER_PRIMARY_KEY_AUTO_INCREMENT + COMMA_SEP
            + AddressesTable.COLUMN_ADDRESS_CITY + TEXT_TYPE + COMMA_SEP
            + AddressesTable.COLUMN_ADDRESS_STREET + TEXT_TYPE + COMMA_SEP
            + AddressesTable.COLUMN_ADDRESS_SUITE + TEXT_TYPE + COMMA_SEP
            + AddressesTable.COLUMN_ADDRESS_ZIPCODE + TEXT_TYPE + COMMA_SEP
            + AddressesTable.COLUMN_ADDRESS_GEO_LAT + REAL_TYPE + COMMA_SEP
            + AddressesTable.COLUMN_ADDRESS_GEO_LNG + REAL_TYPE + COMMA_SEP
            + CompaniesTable.COLUMN_NULLABLE + TEXT_TYPE
            + ")";

    String DELETE_TABLE_ADDRESSES = "DROP TABLE IF EXISTS "
            + AddressesTable.TABLE_NAME;

}
