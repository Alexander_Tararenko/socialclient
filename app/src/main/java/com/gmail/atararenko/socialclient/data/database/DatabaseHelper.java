package com.gmail.atararenko.socialclient.data.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "database.db";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL.CREATE_TABLE_USERS);
        db.execSQL(SQL.CREATE_TABLE_COMPANIES);
        db.execSQL(SQL.CREATE_TABLE_ADDRESSES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL.DELETE_TABLE_USERS);
        db.execSQL(SQL.DELETE_TABLE_COMPANIES);
        db.execSQL(SQL.DELETE_TABLE_ADDRESSES);
        onCreate(db);
    }

}
