package com.gmail.atararenko.socialclient.presentation.view.fragment;

import android.Manifest;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gmail.atararenko.socialclient.R;
import com.gmail.atararenko.socialclient.model.PostModel;
import com.gmail.atararenko.socialclient.presentation.presenter.IPostListPresenter;
import com.gmail.atararenko.socialclient.presentation.presenter.PostListPresenter;
import com.gmail.atararenko.socialclient.presentation.view.IPostListView;
import com.gmail.atararenko.socialclient.presentation.view.adapter.ViewPagerAdapter;
import com.robohorse.pagerbullet.PagerBullet;

import java.io.IOException;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class PostListFragment extends Fragment implements IPostListView {

    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 101;
    private static final int REQUEST_PERMISSION_SETTING = 201;

    private ImageButton mButtonLog;
    private ImageView mImageView;
    private IPostListPresenter mPresenter;
    private OnPostListFragmentListener mCallback;
    private ProgressBar mSpinnerSaveLog;
    private ProgressBar mSpinnerDownloadPosts;
    private View mViewPagerAlternativeLayout;
    private PagerBullet mViewPager;
    private ViewPagerAdapter mAdapter;
    private TextView mTextViewErrorMessage;

    public PostListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post_list, container, false);
        mImageView = (ImageView) view.findViewById(R.id.image_background);
        initSystemWallpaper();
        mButtonLog = (ImageButton) view.findViewById(R.id.button_log);
        mButtonLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPresenter != null) {
                    mPresenter.saveLog();
                }
            }
        });
        mViewPagerAlternativeLayout = view.findViewById(R.id.view_pager_alternative_layout);
        mSpinnerSaveLog = (ProgressBar) view.findViewById(R.id.save_log_spinner);
        mSpinnerDownloadPosts = (ProgressBar) view.findViewById(R.id.posts_spinner);
        mTextViewErrorMessage = (TextView) view.findViewById(R.id.posts_error_message_text_view);
        mTextViewErrorMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadPosts();
            }
        });
        mViewPager = (PagerBullet) view.findViewById(R.id.view_pager_posts);
        mViewPager.setIndicatorTintColorScheme(
                getResources().getColor(R.color.colorAccent),
                getResources().getColor(R.color.colorPrimary));
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
    }

    @Override
    public void showPosts(List<PostModel> posts) {
        if (mViewPager != null) {
            if (posts != null && posts.size() > 0) {
                if (mAdapter == null) {
                    mAdapter = new ViewPagerAdapter(getActivity(), posts);
                    mAdapter.setCallback(new ViewPagerAdapter.OnPostClickListener() {
                        @Override
                        public void onClick(PostModel post) {
                            mCallback.onPostCLick(post);
                        }
                    });
                } else {
                    mAdapter.setPosts(posts);
                }
                mViewPager.setAdapter(mAdapter);
                mViewPager.setCurrentItem(0);
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPostListFragmentListener) {
            this.mCallback = (OnPostListFragmentListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mCallback = null;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        downloadPosts();
    }

    @Override
    public void requestWriteExternalStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            showPermissionDeniedSnackBar();
        } else {
            this.requestPermissions(
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPresenter.saveLog();
                } else {
                    showPermissionDeniedSnackBar();
                }
                return;
            }
        }
    }

    @Override
    public void showSaveLogProgress() {
        mButtonLog.setVisibility(View.GONE);
        mSpinnerSaveLog.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideSaveLogProgress() {
        mButtonLog.setVisibility(View.VISIBLE);
        mSpinnerSaveLog.setVisibility(View.GONE);
    }

    @Override
    public void showDownloadingPostProgress() {
        mViewPager.setVisibility(View.GONE);
        mViewPagerAlternativeLayout.setVisibility(View.VISIBLE);
        mSpinnerDownloadPosts.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideDownloadingPostProgress() {
        mViewPager.setVisibility(View.VISIBLE);
        mViewPagerAlternativeLayout.setVisibility(View.GONE);
        mSpinnerDownloadPosts.setVisibility(View.GONE);
    }

    @Override
    public void showDownloadingPostErrorMessage() {
        mViewPagerAlternativeLayout.setVisibility(View.VISIBLE);
        mTextViewErrorMessage.setVisibility(View.VISIBLE);
        mSpinnerDownloadPosts.setVisibility(View.GONE);
    }

    private void downloadPosts() {
        mTextViewErrorMessage.setVisibility(View.GONE);
        mViewPagerAlternativeLayout.setVisibility(View.GONE);
        if (mPresenter == null) {
            mPresenter = new PostListPresenter(this, getActivity());
        }
        mPresenter.getPosts();
    }

    public void showPermissionDeniedSnackBar() {
        Snackbar.make(getActivity().findViewById(android.R.id.content),
                R.string.permission_denied,
                Snackbar.LENGTH_LONG)
                .setAction(R.string.settings, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getContext().getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                    }
                })
                .show();
    }

    private void initSystemWallpaper() {
        if (mImageView == null) {
            return;
        }
        WallpaperManager manager = WallpaperManager.getInstance(getActivity());
        Drawable backgroundImage;
        if (manager.getWallpaperInfo() == null) {
            backgroundImage = manager.getDrawable();
        } else {
            // user likes live wallpapers
            // set default wallpaper instead
            try {
                backgroundImage = Drawable.createFromStream(
                        getActivity().getAssets().open("android-n-stock-wallpaper.jpg"), null);
            } catch (IOException e) {
                e.printStackTrace();
                backgroundImage = manager.getDrawable();
            }
        }
        mImageView.setImageDrawable(backgroundImage);
    }

    public interface OnPostListFragmentListener {

        void onPostCLick(PostModel post);

    }

}
