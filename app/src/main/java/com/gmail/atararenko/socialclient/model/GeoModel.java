package com.gmail.atararenko.socialclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GeoModel {

    private double lat;
    private double lng;

    public GeoModel() {
    }

    @JsonProperty("lat")
    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    @JsonProperty("lng")
    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GeoModel geoModel = (GeoModel) o;

        if (Double.compare(geoModel.lat, lat) != 0) return false;
        return Double.compare(geoModel.lng, lng) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(lat);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(lng);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "GeoModel{" +
                "lat=" + lat +
                ", lng=" + lng +
                '}';
    }
}
