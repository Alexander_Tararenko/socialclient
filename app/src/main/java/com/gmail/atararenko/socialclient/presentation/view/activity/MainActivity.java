package com.gmail.atararenko.socialclient.presentation.view.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.gmail.atararenko.socialclient.R;
import com.gmail.atararenko.socialclient.model.PostModel;
import com.gmail.atararenko.socialclient.presentation.view.fragment.PostListFragment;
import com.gmail.atararenko.socialclient.presentation.view.fragment.UserDetailsFragment;

public class MainActivity extends AppCompatActivity implements PostListFragment.OnPostListFragmentListener,
        UserDetailsFragment.OnFragmentInteractionListener {

    private static final String FRAGMENT_TAG = "unique tag";
    private static final String FRAGMENT_BACK_STACK_TAG = "Fragment back stack tag";

    private Fragment mFragment;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        if (mFragment == null) {
            mFragment = new PostListFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .add(R.id.fragment, mFragment, FRAGMENT_TAG)
                    .commit();
        }
    }

    @Override
    public void onPostCLick(PostModel post) {
        showContactDetailsFragment(post);
    }

    @Override
    public void openEmail(String email) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("mailto:" + email));
        startActivity(intent);
    }

    @Override
    public void openDialer(String phone) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phone));
        startActivity(intent);
    }

    @Override
    public void openBrowser(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("http://www." + url));
        startActivity(intent);
    }

    @Override
    public void openMaps(double lat, double lng) {
        Uri gmmIntentUri = Uri.parse("geo:" + lat + "," + lng);
        Intent intent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        startActivity(intent);
    }

    private void showContactDetailsFragment(PostModel post) {
        mFragment = UserDetailsFragment.newInstance(post);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment, mFragment, FRAGMENT_TAG)
                .addToBackStack(FRAGMENT_BACK_STACK_TAG)
                .commit();
    }

    private void showPostListFragment() {
        mFragment = new PostListFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment, mFragment, FRAGMENT_TAG)
                .addToBackStack(FRAGMENT_BACK_STACK_TAG)
                .commit();
    }

}
