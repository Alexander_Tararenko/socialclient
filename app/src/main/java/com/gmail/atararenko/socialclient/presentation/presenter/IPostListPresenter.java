package com.gmail.atararenko.socialclient.presentation.presenter;

public interface IPostListPresenter {

    void saveLog();

    void getPosts();

}
