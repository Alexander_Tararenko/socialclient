package com.gmail.atararenko.socialclient.presentation.view;

import com.gmail.atararenko.socialclient.model.UserModel;

public interface IUserDetailsView {

    void showUser(UserModel user);

    void showConnectionErrorMessage();

    void showSaveToDBSuccessMessage();

}
