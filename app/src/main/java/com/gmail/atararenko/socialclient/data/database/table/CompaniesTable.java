package com.gmail.atararenko.socialclient.data.database.table;

public interface CompaniesTable {

    String TABLE_NAME = "companies";
    String COLUMN_COMPANY_NAME = "name";
    String COLUMN_COMPANY_CATCHPHRASE = "catchphrase";
    String COLUMN_COMPANY_BS = "bs";

    String COLUMN_NULLABLE = "nullable";

}
