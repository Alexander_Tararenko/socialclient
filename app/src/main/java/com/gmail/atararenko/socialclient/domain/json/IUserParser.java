package com.gmail.atararenko.socialclient.domain.json;

import com.gmail.atararenko.socialclient.model.UserModel;

import java.io.IOException;

public interface IUserParser {

    UserModel parse(String url) throws IOException;

}
