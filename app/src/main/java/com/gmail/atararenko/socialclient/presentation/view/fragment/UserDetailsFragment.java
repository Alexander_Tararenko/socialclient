package com.gmail.atararenko.socialclient.presentation.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gmail.atararenko.socialclient.R;
import com.gmail.atararenko.socialclient.model.AddressModel;
import com.gmail.atararenko.socialclient.model.GeoModel;
import com.gmail.atararenko.socialclient.model.PostModel;
import com.gmail.atararenko.socialclient.model.UserModel;
import com.gmail.atararenko.socialclient.presentation.presenter.IUserDetailsPresenter;
import com.gmail.atararenko.socialclient.presentation.presenter.UserDetailsPresenter;
import com.gmail.atararenko.socialclient.presentation.view.IUserDetailsView;

public class UserDetailsFragment extends Fragment implements IUserDetailsView {

    private static final String ARG_POST = "post";

    private PostModel mPost;
    private UserModel mUser;
    private OnFragmentInteractionListener mListener;
    private TextView mPostNumber;
    private TextView mName;
    private TextView mNickname;
    private TextView mEmail;
    private TextView mWebsite;
    private TextView mPhone;
    private TextView mAddress;
    private FloatingActionButton mButtonSaveToDB;
    private IUserDetailsPresenter mPresenter;

    public UserDetailsFragment() {
    }

    public static UserDetailsFragment newInstance(PostModel post) {
        UserDetailsFragment fragment = new UserDetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_POST, post);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPost = getArguments().getParcelable(ARG_POST);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_user_details, container, false);
        mPostNumber = (TextView) rootView.findViewById(R.id.post_number);
        mName = (TextView) rootView.findViewById(R.id.name);
        mNickname = (TextView) rootView.findViewById(R.id.nickname);
        mEmail = (TextView) rootView.findViewById(R.id.email);
        mWebsite = (TextView) rootView.findViewById(R.id.website);
        mPhone = (TextView) rootView.findViewById(R.id.phone);
        mAddress = (TextView) rootView.findViewById(R.id.address);
        mButtonSaveToDB = (FloatingActionButton) rootView.findViewById(R.id.fab_save_to_db);
        initListeners();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(getActivity().getString(R.string.contact_details_title) + mPost.getUserId());
        }
        mPostNumber.setText(Long.toString(mPost.getId()));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        downloadUserData();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void showUser(UserModel user) {
        if (user != null) {
            mUser = user;
            fillTextView(mName, user.getName());
            fillTextView(mNickname, user.getUsername());
            fillTextView(mEmail, user.getEmail());
            fillTextView(mWebsite, user.getWebsite());
            fillTextView(mPhone, user.getPhone());
            fillTextView(mAddress, user.getAddress().toString());
        }
    }

    @Override
    public void showConnectionErrorMessage() {
        Snackbar.make(getActivity().findViewById(android.R.id.content),
                R.string.internet_error_message,
                Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void showSaveToDBSuccessMessage() {
        Snackbar.make(getActivity().findViewById(android.R.id.content),
                R.string.save_to_db_success_message,
                Snackbar.LENGTH_SHORT)
                .show();
    }

    private void downloadUserData() {
        if (mPresenter == null) {
            mPresenter = new UserDetailsPresenter(this, getContext());
        }
        mPresenter.getUserData(mPost.getUserId());
    }

    private void fillTextView(TextView view, String text) {
        if (view != null && text != null) {
            view.setText(text);
        }
    }

    private void initListeners() {
        mEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    String email = mUser.getEmail();
                    if (email != null) {
                        mListener.openEmail(email);
                    }
                }
            }
        });
        mPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    String phone = mUser.getPhone();
                    if (phone != null) {
                        mListener.openDialer(phone);
                    }
                }
            }
        });
        mWebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    String website = mUser.getWebsite();
                    if (website != null) {
                        mListener.openBrowser(website);
                    }
                }
            }
        });
        mAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    AddressModel address = mUser.getAddress();
                    if (address != null && address.getGeo() != null) {
                        GeoModel geo = address.getGeo();
                        mListener.openMaps(geo.getLat(), geo.getLng());
                    }
                }
            }
        });
        mButtonSaveToDB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPresenter != null && mUser != null) {
                    mPresenter.saveUserToDatabase(mUser);
                }
            }
        });
    }

    public interface OnFragmentInteractionListener {

        void openEmail(String email);

        void openDialer(String phone);

        void openBrowser(String url);

        void openMaps(double lat, double lng);

    }
}
