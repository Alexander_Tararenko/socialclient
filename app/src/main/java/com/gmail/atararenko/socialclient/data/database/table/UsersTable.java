package com.gmail.atararenko.socialclient.data.database.table;

public interface UsersTable {

    String TABLE_NAME = "users";
    String COLUMN_USER_ID = "user_id";
    String COLUMN_USER_NAME = "name";
    String COLUMN_USER_USERNAME = "username";
    String COLUMN_USER_EMAIL = "email";
    String COLUMN_USER_PHONE = "phone";
    String COLUMN_USER_WEBSITE = "website";
    String COLUMN_USER_ADDRESS = "address";
    String COLUMN_USER_COMPANY = "company";

    String COLUMN_NULLABLE = "nullable";

}
