package com.gmail.atararenko.socialclient.domain.json;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gmail.atararenko.socialclient.domain.web.HttpRetriever;
import com.gmail.atararenko.socialclient.model.UserModel;

import java.io.IOException;

public class UserParser implements IUserParser {

    @Override
    public UserModel parse(String url) throws IOException {
        String jsonString = HttpRetriever.retrieve(url, 15);
        ObjectMapper mapper = new ObjectMapper();
        UserModel user = mapper.readValue(jsonString, new TypeReference<UserModel>() {
        });
        return user;
    }
}
