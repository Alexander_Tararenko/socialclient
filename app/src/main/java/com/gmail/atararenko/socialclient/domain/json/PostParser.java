package com.gmail.atararenko.socialclient.domain.json;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gmail.atararenko.socialclient.domain.web.HttpRetriever;
import com.gmail.atararenko.socialclient.model.PostModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PostParser implements IPostParser {

    public PostParser() {
    }

    @Override
    public List<PostModel> parse(String url) throws IOException {
        String jsonString = HttpRetriever.retrieve(url, 15);
        ObjectMapper mapper = new ObjectMapper();
        List<PostModel> posts = mapper.readValue(jsonString, new TypeReference<ArrayList<PostModel>>() {
        });
        return posts;
    }

}
