package com.gmail.atararenko.socialclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AddressModel {

    private String street;
    private String suite;
    private String city;
    private String zipcode;
    private GeoModel geo;

    @JsonProperty("street")
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @JsonProperty("suite")
    public String getSuite() {
        return suite;
    }

    public void setSuite(String suite) {
        this.suite = suite;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @JsonProperty("zipcode")
    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    @JsonProperty("geo")
    public GeoModel getGeo() {
        return geo;
    }

    public void setGeo(GeoModel geo) {
        this.geo = geo;
    }

    @Override
    public String toString() {
        return city + ", " + street + ", " + suite;
    }
}
