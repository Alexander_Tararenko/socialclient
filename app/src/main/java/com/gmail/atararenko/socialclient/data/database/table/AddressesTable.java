package com.gmail.atararenko.socialclient.data.database.table;

public interface AddressesTable {

    String TABLE_NAME = "addresses";
    String COLUMN_ADDRESS_ID = "_id";
    String COLUMN_ADDRESS_CITY = "city";
    String COLUMN_ADDRESS_STREET = "street";
    String COLUMN_ADDRESS_SUITE = "suite";
    String COLUMN_ADDRESS_ZIPCODE = "zipcode";
    String COLUMN_ADDRESS_GEO_LAT = "geo_lat";
    String COLUMN_ADDRESS_GEO_LNG = "geo_lng";

    String COLUMN_NULLABLE = "nullable";

    String[] COLUMNS = new String[]{
            COLUMN_ADDRESS_ID,
            COLUMN_ADDRESS_CITY,
            COLUMN_ADDRESS_STREET,
            COLUMN_ADDRESS_SUITE,
            COLUMN_ADDRESS_ZIPCODE,
            COLUMN_ADDRESS_GEO_LAT,
            COLUMN_ADDRESS_GEO_LNG,
            COLUMN_NULLABLE
    };
}
