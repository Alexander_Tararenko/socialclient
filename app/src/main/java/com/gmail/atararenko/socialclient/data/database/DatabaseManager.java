package com.gmail.atararenko.socialclient.data.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.gmail.atararenko.socialclient.data.database.table.AddressesTable;
import com.gmail.atararenko.socialclient.data.database.table.CompaniesTable;
import com.gmail.atararenko.socialclient.data.database.table.UsersTable;
import com.gmail.atararenko.socialclient.model.AddressModel;
import com.gmail.atararenko.socialclient.model.CompanyModel;
import com.gmail.atararenko.socialclient.model.UserModel;

public class DatabaseManager {

    private Context mContext;
    private SQLiteDatabase mDatabase;

    public DatabaseManager(Context mContext) {
        this.mContext = mContext;
        openConnection();
    }

    public long addUser(UserModel user) {
        ContentValues values = generateValuesFromUserModel(user);
        if (user.getCompany() != null) {
            addCompany(user.getCompany());
        }
        if (user.getAddress() != null) {
            addAddress(user.getAddress());
        }
        return mDatabase.replace(UsersTable.TABLE_NAME, UsersTable.COLUMN_NULLABLE, values);
    }

    private long addCompany(CompanyModel company) {
        return mDatabase.replace(CompaniesTable.TABLE_NAME,
                CompaniesTable.COLUMN_NULLABLE,
                generateValuesFromCompanyModel(company));
    }

    private long addAddress(AddressModel address) {
        long id = checkAddressInDb(address);
        ContentValues values = generateValuesFromAddressModel(address);
        if (id != -1) {
            values.put(AddressesTable.COLUMN_ADDRESS_ID, id);
        }
        return mDatabase.replace(AddressesTable.TABLE_NAME,
                AddressesTable.COLUMN_NULLABLE,
                values);
    }

    private long checkAddressInDb(AddressModel address) {
        Cursor cursor = mDatabase.query(AddressesTable.TABLE_NAME,
                AddressesTable.COLUMNS,
                AddressesTable.COLUMN_ADDRESS_CITY + " = ?"
                        + " AND " + AddressesTable.COLUMN_ADDRESS_STREET + " = ?"
                        + " AND " + AddressesTable.COLUMN_ADDRESS_SUITE + " = ?",
                new String[]{address.getCity(), address.getStreet(), address.getSuite()},
                null, null, null);
        if (cursor == null || !cursor.moveToFirst()) {
            return -1;
        } else {
            return cursor.getLong(cursor.getColumnIndex(AddressesTable.COLUMN_ADDRESS_ID));
        }
    }

    private ContentValues generateValuesFromUserModel(UserModel user) {
        ContentValues values = new ContentValues();
        values.put(UsersTable.COLUMN_USER_ID, user.getId());
        values.put(UsersTable.COLUMN_USER_NAME, user.getName());
        values.put(UsersTable.COLUMN_USER_USERNAME, user.getUsername());
        values.put(UsersTable.COLUMN_USER_EMAIL, user.getEmail());
        values.put(UsersTable.COLUMN_USER_PHONE, user.getPhone());
        values.put(UsersTable.COLUMN_USER_WEBSITE, user.getWebsite());
        return values;
    }

    private ContentValues generateValuesFromCompanyModel(CompanyModel company) {
        ContentValues values = new ContentValues();
        values.put(CompaniesTable.COLUMN_COMPANY_NAME, company.getName());
        values.put(CompaniesTable.COLUMN_COMPANY_CATCHPHRASE, company.getCatchPhrase());
        values.put(CompaniesTable.COLUMN_COMPANY_BS, company.getBs());
        return values;
    }

    private ContentValues generateValuesFromAddressModel(AddressModel address) {
        ContentValues values = new ContentValues();
        values.put(AddressesTable.COLUMN_ADDRESS_CITY, address.getCity());
        values.put(AddressesTable.COLUMN_ADDRESS_STREET, address.getStreet());
        values.put(AddressesTable.COLUMN_ADDRESS_SUITE, address.getSuite());
        values.put(AddressesTable.COLUMN_ADDRESS_ZIPCODE, address.getZipcode());
        values.put(AddressesTable.COLUMN_ADDRESS_GEO_LAT, address.getGeo().getLat());
        values.put(AddressesTable.COLUMN_ADDRESS_GEO_LNG, address.getGeo().getLng());
        return values;
    }

    public void openConnection() {
        if (mDatabase == null) {
            mDatabase = new DatabaseHelper(mContext).getWritableDatabase();
        } else if (!mDatabase.isOpen()) {
            mDatabase = new DatabaseHelper(mContext).getWritableDatabase();
        }
    }

    public void closeConnection() {
        mDatabase.close();
    }


}
