package com.gmail.atararenko.socialclient.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PostModel implements Parcelable {

    public static final Parcelable.Creator<PostModel> CREATOR = new Creator<PostModel>() {
        @Override
        public PostModel createFromParcel(Parcel parcel) {
            return new PostModel(parcel);
        }

        @Override
        public PostModel[] newArray(int size) {
            return new PostModel[size];
        }
    };
    private long id;
    private long userId;
    private String title;
    private String body;

    public PostModel() {
    }

    public PostModel(long id, long userId, String title, String body) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.body = body;
    }

    public PostModel(Parcel in) {
        String[] data1 = new String[2];
        long[] data2 = new long[2];
        in.readStringArray(data1);
        in.readLongArray(data2);
        this.id = data2[0];
        this.userId = data2[1];
        this.title = data1[0];
        this.body = data1[0];
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLongArray(new long[]{id, userId});
        parcel.writeStringArray(new String[]{title, body});
    }

    @JsonProperty("id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @JsonProperty("userId")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("body")
    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PostModel postModel = (PostModel) o;

        if (id != postModel.id) return false;
        if (userId != postModel.userId) return false;
        if (title != null ? !title.equals(postModel.title) : postModel.title != null) return false;
        return body != null ? body.equals(postModel.body) : postModel.body == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (userId ^ (userId >>> 32));
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (body != null ? body.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "\n\nPostModel{" +
                "id=" + id + '\n' +
                ", userId=" + userId + '\n' +
                ", title='" + title + '\'' + '\n' +
                ", body='" + body + '\'' +
                "}";
    }
}
